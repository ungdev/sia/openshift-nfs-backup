#!/bin/bash -e

echo "MOUNT_DIRECTORY $MOUNT_DIRECTORY"

MOUNT_DIRECTORY=${MOUNT_DIRECTORY:-"/srv/backup"}

for volume in $(ls ${MOUNT_DIRECTORY}); do
  mount_path=${MOUNT_DIRECTORY}/${volume}
  echo "Unmount ${mount_path}"
  umount ${mount_path}
  rmdir ${mount_path}
done

oc logout

echo "Finished !"
