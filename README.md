# OpenShift NFS Borgmatic Backup

From a directory of NFS folders, maps those which are tagged backup=nfs and mount them in a temporary directory
The goal is to backup only production volumes

It is a collection of script used for the hooks before and after a borgbackup save

## Requirements

`oc` installed on the NFS Host

## Summary

Collection of scripts for borgmatic hooks

Before Script: Mount all volumes in a folder as bind for all persistant volume claims which has the label backup=nfs
After Script: Unmount the volume and removes the folder

To add a volume marked as backup, just type `oc label pvc <PVC-NAME> backup=nfs`

Mounts are used instead of symbolic links because Borg doesn't follow symbolic links

## How to use ?

### Service account creation

It's used to allow getting all volumes from the cli with specific token.

1. Create the Cluster Role

```
oc create -f rbac/clusterrole.yml
```

2. Create the Service Account:

```
oc create -f rbac/serviceaccount.yml
```

3. Add the permission:

```
oc adm policy add-cluster-role-to-user nfs-backup system:serviceaccount:$(oc project -q):nfs-backup
```

Then get the token associated (you can find it in the openshift secrets) and put them in `~/.nfs-backup` or the place you want (you'll have to fill in the)

### Configure it on your host

On your NFS host, install borgbackup, then copy and edit the following files :

systemd/borgbackup-okd.* to /etc/systemd/system/

okd-repo.conf to /etc/borgmatic.d/okd-repo.conf

### Compulsory environnement variables

In the okd-repo.conf, you'll need to configure this var

OPENSHIFT_URL: URL of the openshift server

### Other variables
MOUNT_DIRECTORY: mount of backups, default `/srv/backup`

OPENSHIFT_DIRECTORY: mount of current volumes, default `/export/openshift`

CREDENTIALS_LOCATION: place of service account token, default `/root/.nfs-backup`